﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication2.Models;
using System.Data.Entity;
using System.Web.Configuration;

namespace WebApplication2.Controllers
{
    public class DoctorDBContext : DbContext
    {
        private static DoctorDBContext doctorDbContext;
        private static string selectedDataBase = WebConfigurationManager.AppSettings["selectedDataBase"];
        private DoctorDBContext(string connectionString) : base(connectionString)
        {

        }
        private DoctorDBContext() 
        {

        }
        public DbSet<Doctor> Doctors { set; get; }
        public DbSet<Specialtie> Specialties { set; get; }
        public DbSet<DoctorsSpecialies> DoctorsSpecialies { set; get; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Doctor>()

                .HasMany(d => d.Specialtie).WithMany(s => s.Doctor);
            modelBuilder.Entity<Specialtie>()
                .HasMany(s => s.Doctor).WithMany(d => d.Specialtie);
           // modelBuilder.Entity<DoctorsSpecialies>().MapToStoredProcedures();
            base.OnModelCreating(modelBuilder);
        }

        // dont return same instance each time as it will disposed each time 
        public static DoctorDBContext getDBInstance()
        {
            if(selectedDataBase != null && !selectedDataBase.Equals(""))
            {
                return new DoctorDBContext(selectedDataBase);

            }
            else
            {
                return new DoctorDBContext(); ;
            }
        }
    }
}