﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication2.Models;
using System.Data.Entity;


namespace WebApplication2.Controllers
{
    public class DoctorRepository
    {
        DoctorDBContext doctorDbContext = DoctorDBContext.getDBInstance();
        public List<Doctor> getDoctors()
        {
            
            return doctorDbContext.Doctors.Include(d => d.Specialtie ).ToList();
        }

        public int InsertDoctor(Doctor doctor)
        {
            // attach specialtie
            if(doctor.Specialtie != null)
            foreach(Specialtie specialtie in doctor.Specialtie)
            {
                doctorDbContext.Specialties.Attach(specialtie);

            }
             
            doctorDbContext.Doctors.Add(doctor);
            doctorDbContext.SaveChanges();
            int doctorID = doctor.ID;
            return doctorID;
        }

        public void UpdateDoctor(Doctor doctor)
        {
             
            Doctor doctorToUpdate = doctorDbContext.Doctors.Include(d => d.Specialtie).FirstOrDefault(d => d.ID == doctor.ID);
            doctorToUpdate.FirstName = doctor.FirstName;
            doctorToUpdate.LastName = doctor.LastName;
            doctorToUpdate.Gender = doctor.Gender;
            // reinit specilties
            doctorToUpdate.Specialtie.Clear();
            // attach specialtie
            foreach (Specialtie specialtie in doctor.Specialtie)
            {
                doctorDbContext.Specialties.Attach(specialtie);

            }
            doctorToUpdate.Specialtie = doctor.Specialtie;
            doctorDbContext.SaveChanges();
        }

        public void DeleteDoctor(Doctor doctor)
        {
           
            Doctor doctorToDelete = doctorDbContext.Doctors.FirstOrDefault(d => d.ID == doctor.ID);
            doctorDbContext.Doctors.Remove(doctorToDelete);

            doctorDbContext.SaveChanges();
        }

    }
}