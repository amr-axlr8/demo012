﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using WebApplication2.Models;

namespace WebApplication2.Controllers
{
    public class SpecialtiesController : ApiController
    {
        private DoctorDBContext db = DoctorDBContext.getDBInstance();

        // GET: api/Specialties
        public IQueryable<Specialtie> GetSpecialties()
        {
            return db.Specialties.Include(d => d.Doctor);
        }

        // GET: api/Specialties/5
        [ResponseType(typeof(Specialtie))]
        public async Task<IHttpActionResult> GetSpecialtie(int id)
        {
            Specialtie specialtie = await db.Specialties.FindAsync(id);
            if (specialtie == null)
            {
                return NotFound();
            }

            return Ok(specialtie);
        }

        // PUT: api/Specialties/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutSpecialtie(int id, Specialtie specialtie)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != specialtie.ID)
            {
                return BadRequest();
            }

            db.Entry(specialtie).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SpecialtieExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Specialties
        [ResponseType(typeof(Specialtie))]
        public async Task<IHttpActionResult> PostSpecialtie(Specialtie specialtie)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Specialties.Add(specialtie);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = specialtie.ID }, specialtie);
        }

        // DELETE: api/Specialties/5
        [ResponseType(typeof(Specialtie))]
        public async Task<IHttpActionResult> DeleteSpecialtie(int id)
        {
            Specialtie specialtie = await db.Specialties.FindAsync(id);
            if (specialtie == null)
            {
                return NotFound();
            }

            db.Specialties.Remove(specialtie);
            await db.SaveChangesAsync();

            return Ok(specialtie);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool SpecialtieExists(int id)
        {
            return db.Specialties.Count(e => e.ID == id) > 0;
        }
    }
}