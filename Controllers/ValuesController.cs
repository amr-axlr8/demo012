﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApplication2.Models ;

namespace WebApplication2.Controllers
{
    ///[Authorize]
    public class ValuesController : ApiController
    {
        DoctorRepository doctorRepo = new DoctorRepository();
        // GET api/values
        public List<Doctor> Get()
        {
            List<Doctor> doctors = doctorRepo.getDoctors();
            return doctors;
            
        }

        // GET api/values/5
        public string Get(int id)
        {
            return "value";
        }


        
       
        // POST api/values
        [HttpPost]
        public int Post(Doctor doctor)
        {   
            int doctorID = doctorRepo.InsertDoctor(doctor);
            return doctorID;

        }

        // PUT api/values
        public void Put(Doctor doctor)
        {
            doctorRepo.UpdateDoctor(doctor);
        }

        // DELETE api/values
        public void Delete(Doctor doctor)
        {
            doctorRepo.DeleteDoctor(doctor);
        }
    }
}
