﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WebApplication2.Models
{
    public class DoctorsSpecialies
    {
        public Doctor Doctor { set; get; }
        public Specialtie Specialtie { set; get; }
        [Key,Column(Order = 1)]
        public int DoctorID { get; set; }

        [Key, Column(Order = 2)]
        public int SpecialtieID { get; set; }

        public DateTime EnrolledDate { get; set; }
    }
}