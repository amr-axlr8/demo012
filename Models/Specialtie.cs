﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApplication2.Models
{
    public class Specialtie
    {
        [Key]
        [DisplayName("ID")]
        public int ID { set; get; }
        public string name { set; get; }
        public IList<Doctor> Doctor { get; set;}
    }
}